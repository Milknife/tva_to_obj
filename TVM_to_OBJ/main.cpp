/* main.cpp
@attention REQUIRES C++17 due to use of std::filesystem
@brief Application is able to convert single files or whole folders of
TrueVision3D TVM format files into the Waveform OBJ format.

Usage:
	Display this text
	./TMV_to_OBJ.exe -h

	The file name extensions separate the single file and batch usage.
	*Single file conversion
	./TMV_to_OBJ.exe TVMFileName.tvm
	./TMV_to_OBJ.exe TVMFileName.tvm OBJFileName.obj

	*Batch Conversion
	./TMV_to_OBJ.exe SourceFolderIsDestinationFolder
	./TMV_to_OBJ.exe SourceFolder -DestinationFolder

Adam Clarke - 2018 10 11
*/

#include <iostream>
#include <filesystem>
#include <memory>
#include <string>

#include "TVM_mesh.h"

/* @brief Enumerator to indicate the argument status
*/
enum argument_type
{
	invalid = 0,
	file = 1,
	folder = 2
};

/***GLOBAL FUNCTION DECLARATIONS***/

/* @summary Sanity checking of arguments to determin single file or batch conversion.
*/
void process_arguments();

/* @brief Check the string ends with given file extension. If a different extension is found,
	returns invalid, if no extension found, returns folder.
   @param The program argument to check for file extension.   
   @param The extension to check for.
   @return emun file/folder/or invalid
*/
argument_type get_argument_type(const std::string& a_argument, const std::string& a_extension);

/* @brief Loads and converts a single TVM to an OBJ.
   @param a_source a file name ending in .tvm
   @param a_destination a file name ending in .obj
*/
void convert_TVM(const std::string& a_source, const std::string& a_destination);

/* @brief Loads and converts a single TVM to an OBJ.
   @param a_source a relative directory path to search for files
   @param a_destination a relative directory path to save files
*/
void batch_conversion(const std::string& a_source, const std::string& a_destination);

/* @brief Displays the usage summary for this application
*/
void display_help();

/* @brief Displays invalid command line error message
*/
void display_error_command_line();

/***GLOBAL VARIABLE DECLARATIONS***/

/* @brief Pronounced eeyyy-ch. literal string for help screen.
*/
const std::string aych = "-h";

/* @brief Local count of command line arguments
*/
size_t argument_count;

/* @brief Dynamic array for storage of command line arguments
*/
std::unique_ptr<std::string[]> command_line_arguments;

/* @brief Global decoder storage
*/
TVM_mesh mesh;

/* @brief Decoder target
*/
std::string source_path;

/* @brief Dynamic array for storage of command line arguments
*/
std::string tvm_extension = ".tvm";

/* @brief Dynamic array for storage of command line arguments
*/
std::string TVM_extension = ".TVM";

/* @brief encoding target
*/
std::string destination_path;

/* @brief Dynamic array for storage of command line arguments
*/
std::string obj_extension = ".obj";

/* @brief Program launch point. Expects a source and destination for conversion
   @param a_arg_count number of command line arguments parsed
   @param a_arg_vector array of char* containing the arguments
*/
int main(int a_arg_count, char* a_arg_vector[])
{
	// Count and copy the command line arguments for simpler usage.
	argument_count = a_arg_count;

	// Too many arguments
	if (argument_count < 2 || argument_count > 3)
	{
		display_error_command_line();
		return 0;
	}

	// Copy out the arguments for string processing
	command_line_arguments = std::make_unique<std::string[]>(argument_count);
	for (size_t index = 0; index < argument_count; ++index)
	{
		command_line_arguments[index] = a_arg_vector[index];
	}

	// If -h found show help
	if (command_line_arguments[1] == aych) // Contentious pronounciation 
	{
		display_help();
	}
	else
	{
		process_arguments();
	}

	// Pause on the way out.
	system("pause");
	return 0;
}

/* @brief Sanity checking of arguments to determine single file or batch conversion.
*/
void process_arguments()
{
	// In the case of two arguments, the first is the exe name.
	// And the second should either be a folder, or file name
	if (argument_count == 2)
	{
		std::cout << "Executing for single argument.\n";
		// Is the first argument a .tvm extension?
		source_path = command_line_arguments[1];
		// Check against lowercase extension
		argument_type source_type = get_argument_type(source_path, tvm_extension);
		// if a period was found, but not '.tvm' check upper case.
		if (source_type == invalid)
		{
			source_type = get_argument_type(source_path, TVM_extension);
		}

		// Use the path type as a switch
		switch (source_type)
		{
			case file:
			{
				// A single TVM discovered. Source and destingation the same, obj, replaces tvm
				destination_path = source_path;
				// Amend the destination as .obj
				destination_path.replace(destination_path.length() - tvm_extension.length(), tvm_extension.length(), obj_extension);
				// Finally convert.
				convert_TVM(source_path, destination_path);
				break;
			}
			case folder:
			{
				// Single folder argument, convert all found tvms to objs
				batch_conversion(source_path, source_path);
				break;
			}
			default: // Invalid file type in argument
			{
				display_error_command_line();
				break;
			}
		}
	}
	// Only other option is a pair of arguments
	else if (argument_count == 3)
	{
		std::cout << "Executing for two arguments.\n";
		// Cache local paths
		source_path = command_line_arguments[1];
		destination_path = command_line_arguments[2];

		// Are both arguments files with correct extensions?
		argument_type source_type = get_argument_type(source_path, tvm_extension);
		if (source_type == invalid) // Check case sensitive when period found
		{
			source_type = get_argument_type(source_path, TVM_extension);
		}
		argument_type destination_type = get_argument_type(destination_path, obj_extension);

		// Both arguments validated file extensions?
		if (source_type == file || destination_type == file)
		{
			convert_TVM(source_path, destination_path);
		}
		// else a pair of folders
		else if (source_type == folder || destination_type == folder)
		{
			batch_conversion(source_path, destination_path);
		}
		else // Bad arguments
		{
			display_error_command_line();
		}
	}
}

/* @brief Check the string ends with given file extension. If a different extension is found,
   returns invalid, if no extension found, returns folder.
   @param The program argument to check for file extension.
   @param The extension to check for.
   @return emun file/folder/or invalid
*/
argument_type get_argument_type(const std::string& a_argument, const std::string& a_extension)
{
	// Is the argument long enough to contain the extension?
	if (a_argument.length() > a_extension.length())
	{
		// Search for extension at the end of the argument
		std::string_view postfix = a_argument;
		std::string_view extension = postfix.substr(a_argument.length() - a_extension.length());

		// If extension found, notify and set type
		if (extension == a_extension)
		{
			std::cout << "File extension " << a_extension << " found \n";
			return file;
		}
	}

	//Search for any extension - if any dots found
	if (a_argument.find(".") != std::string::basic_string::npos)
	{
		std::cout << "Other extension found.\n";
		return invalid;
	}
	else // Free of extension
	{
		std::cout << "Folder name valid: " << a_argument <<"\n";
		return folder;
	}
}

/* @brief Loads and converts a single TVM to an OBJ.
   @param a_source a file name ending in .tvm
   @param a_destination a file name ending in .obj
*/
void convert_TVM(const std::string& a_source, const std::string& a_destination) 
{
	std::cout << "Converting " << a_source << " to " << a_destination << "\n";
	// Are both arguments files with correct extensions?
	argument_type source_type = get_argument_type(a_source, tvm_extension);
	if (source_type == invalid) // Check case sensitive when period found
	{
		source_type = get_argument_type(a_source, TVM_extension);
	}
	argument_type destination_type = get_argument_type(a_destination, obj_extension);

	// Only convert of both file paths valid
	if (source_type == file && destination_type == file)
	{
		// Loaded
		if (mesh.read_from_tvm(a_source))
		{
			// Written
			if (mesh.write_to_obj(a_destination)) 
			{
				// User confirmation
				std::cout << "Saved: " << a_destination << "\n"; 
			}
			else
			{
				// Failed save
				std::cout << a_destination << " has failed to save.\n";
			}
		}
		else
		{
			// Failed Load
			std::cout << a_destination << " has failed to load.\n";
		}
	}
	else
	{
		// Failed to parse paths
		std::cout << "Conversion not valid!\n";
	}
}

/* @brief Loads and converts all tvms in source folder and saves them as obj in destination folder
   @param a_source a relative directory path to search for files
   @param a_destination a relative directory path to save files
*/
void batch_conversion(const std::string& a_source, const std::string& a_destination) 
{
	std::filesystem::path source = a_source;
	std::filesystem::path destination = a_destination;

	// Notify the user
	std::cout << "Directories for batch conversion\n" << source << " to " << destination << "\n";

	// if both valid directory objects
	if (std::filesystem::is_directory(source) && std::filesystem::is_directory(destination))
	{
		// C++17 required here for iteration of file system
		// Iterate directory, check for tvms, conver to objs at destination
		std::wstring target_file_path;
		std::string input_path;
		std::string output_path;
		for (auto& file_in_directory : std::filesystem::directory_iterator(source))
		{
			// std::filesystem::path is wchar_t, need to convert them to strings
			// and check file extensions
			target_file_path = file_in_directory.path().c_str();

			// build the file input string
			//https://stackoverflow.com/questions/2573834/c-convert-string-or-char-to-wstring-or-wchar-t
			input_path = std::string(target_file_path.begin(), target_file_path.end());

			// Check found file is a TVM
			argument_type source_type = get_argument_type(input_path, tvm_extension);
			// Check case sensitive when period found
			if (source_type == invalid) source_type = get_argument_type(input_path, TVM_extension); 
			
			// if not a valid tvm, move onto next file
			if (source_type != file)
			{
				std::cout << input_path << " was not a valid tvm.\n";
				continue;
			}

			// Build the output.obj string from...
			// The input file's name
			target_file_path = file_in_directory.path().filename().c_str();
			// Without the extension
			output_path = std::string(target_file_path.begin(), target_file_path.end() - obj_extension.length());
			// Append the correct extension
			output_path += obj_extension;
			// Preappend the path.
			output_path = a_destination + "\\" + output_path;
			// Convert!
			convert_TVM(input_path, output_path);
		}
	}
	else
	{
		std::cout << "Directories not found";
	}
}

/* @brief Displays the usage summary for this application
*/
void display_help()
{
	std::cout <<
		"Usage:" << std::endl <<
		"\t" << "Display this help:" << std::endl <<
		"\t" << "TMV_to_OBJ.exe -h" << std::endl <<
		"\t" << std::endl <<
		"\t" << "The file name extensions separate the single file and batch usage." << std::endl <<
		"\t" << std::endl <<
		"\t" << "Single file conversion:" << std::endl <<
		"\t" << "TMV_to_OBJ.exe TVMFileName.tvm" << std::endl <<
		"\t" << "TMV_to_OBJ.exe TVMFileName.tvm OBJFileName.obj" << std::endl <<
		"\t" << std::endl <<
		"\t" << "Batch Conversion:" << std::endl <<
		"\t" << "TMV_to_OBJ.exe -SourceFolderIsDestinationFolder" << std::endl <<
		"\t" << "TMV_to_OBJ.exe SourceFolder DestinationFolder" << std::endl;
}

/* @brief Displays error message for bad arguments
*/
void display_error_command_line()
{
	std::cout << "Source and destination required. -h for help." << std::endl;
}
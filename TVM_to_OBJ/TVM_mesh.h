/* mesh.cpp
@brief Stores a decoded mesh data from the TVM format in
a standard set of verticies and triangle indicies to be
written as an obj on demand.

Usage: 
  TVM_mesh my_mesh;
  if(my_mesh.read_from_tvm("A tvm file name"))
    if(my_mesh.write_to_obj("An obj file name"))
	  //Saved Successfully

Adam Clarke - 2018 10 11
*/

#pragma once
#include <memory>
#include <string>
#include <vector>

#include "vertex.h"

#ifndef TVA_MESH_H
#define TVA_MESH_H

// Typedef needed for some compilers
typedef short int16_t;

class TVM_mesh
{
public:
	/* @breif Constructor zeros all values
	*/
	TVM_mesh();

	/* @breif Constructor zeros all values
	   @param Path and file to load into mesh
	   @return True if the mesh has loaded successfully
	*/
	bool read_from_tvm(const std::string& a_path_and_file_name);

	/* @breif Outputs current mesh data as obj file
       @param Path to save file to
       @return False if no mesh data or file unable to be written to
	*/
	bool write_to_obj(const std::string& a_path_and_file_name);

private:
	/* @brief Number of verticies
	*/
	size_t vertex_count;
	/* @brief Buffer of loaded vertices
	*/
	std::unique_ptr<vertex[]> verticies;

	/* @brief Number of triangles
	*/
	size_t triangle_count;
	/* @brief Buffer of triangle indices
	*/
	std::unique_ptr<int16_t[]> indicies;

	/* @brief Known list of char based delimiters from the TVM format
	   stored as a vector 
	*/
	static const std::vector<std::string> delimiters;

	/* @brief Enumerated delimiter indicies for the two
	   that we know / care about */
	enum delimiter_index
	{
		MVER = 3,
		MI16 = 4
	};

	/* @brief Struct for reading data from start to end between
	   particular delimiters,for the vertex and indicie buffers.
	*/
	struct sentinel
	{
		char* start_location;
		char* end_location;
		size_t length;
	}
	MVER_buffer_location,  // Vertex buffer sentinel
	MI16_buffer_location;  // Index buffer sentinel

	/* @brief Initializes the sentinals
	   @return False if any sentinels fail to initialize
	*/
	bool initialze_sentinels(std::unique_ptr<char[]>& a_file_buffer,
							 const size_t a_file_length_bytes);

	/* @brief look though the file buffer for the matching string location
   @param The sentinel to populate
   @param The index of the 'start' delimiter
   @param Pointer to the file buffer
   @param Length of the file buffer
	   @return false if unsuccessful in finding the inner buffer
	*/
	bool find_delimiters(sentinel& a_sentinal_data,
						 const delimiter_index a_index, 
						 std::unique_ptr<char[]>& a_file_buffer,
						 const size_t a_file_length_bytes);
};

#endif // !TVA_MESH_H


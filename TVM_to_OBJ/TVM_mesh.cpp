#include "TVM_mesh.h"

#include <fstream>
#include <iostream>

/* @breif Constructor zeros all values
*/
TVM_mesh::TVM_mesh()
{
	vertex_count = 0;
	verticies = nullptr;

	triangle_count = 0;
	indicies = nullptr;

	MVER_buffer_location = { nullptr, nullptr, 0 };
	MI16_buffer_location = { nullptr, nullptr, 0 };
}

/* @breif Constructor zeros all values
   @param Path and file to load into mesh
   @return True if the mesh has loaded successfully
*/
bool TVM_mesh::read_from_tvm(const std::string& a_path_and_file_name)
{
	// In case of reuse, reset vertices, indicies, and sentinels
	if (verticies)
	{
		vertex_count = 0;
		verticies.reset();
	}

	if (indicies)
	{
		triangle_count = 0;
		indicies.reset();
	}

	MVER_buffer_location = { nullptr, nullptr, 0 };
	MI16_buffer_location = { nullptr, nullptr, 0 };

	//** Step 1 - Open and read the content **//
	// Input file stream object only needed during loading
	// -Given a unique ifsteam for threading
	std::ifstream in_file_stream;
	// Open as binary and jump to the end of the file (for the size)
	in_file_stream.open(a_path_and_file_name, std::ios::binary | std::ios::ate);
	// Check we have file access
	if (!in_file_stream.is_open())
	{
		std::cout << "File not found: " << a_path_and_file_name << "\n";
		return false;
	}
	// else we have the file length
	size_t file_length_bytes = in_file_stream.tellg();
	// Move the read buffer to the start of the file
	// ie. seek good bit at zero.
	in_file_stream.seekg(0);

	// Build local storage for the binary file
	auto file_buffer = std::make_unique<char[]>(file_length_bytes);
	// Copy data from the buffer - Get needed to read to char*
	in_file_stream.read(file_buffer.get(), file_length_bytes);
	// Close off the buffer
	in_file_stream.close();

	//** Step 2 - Find the start and end points of the vertex and triangle streams **//
	if (!initialze_sentinels(file_buffer, file_length_bytes))
	{
		std::cout << "Sentinels failed to find buffer location.\n";
		return false;
	}

	//** Step 3 - Store the data from the buffer, using the sentinels, locally **//
	// 3.A - The first integer in the vertex buffer is the number of bytes to follow.
	// The bytes that follow are sets of 8 floats as per our vertex union.
	//  hence we divide this by 32 (size_of_vertex) for a final vertex count.
	// Reinterpreting the char pointer as an int pointer then dereferencing it for value
	vertex_count =
		*(reinterpret_cast<int*>(MVER_buffer_location.start_location)) / size_of_vertex;

	// Initialize our vertex buffer
	verticies = std::make_unique<vertex[]>(vertex_count);
	// Copy from the file buffer as vertex unions
	int vertex_index = 0;
	for (char* index = MVER_buffer_location.start_location + 4; // Skip the byte count
		index < MVER_buffer_location.end_location;				// first byte of next delimiter
		index += sizeof(vertex),								// Iterate through bytes by size of vertex
		++vertex_index)											// Iterate storage index
	{
		// 8 floats per vertex
		for (int i = 0; i < floats_per_vertex; ++i)
		{
			// Step though the vertex by size of float
			verticies[vertex_index].float_data[i] =
				*(reinterpret_cast<float*> (index + i * size_of_float));
		}
	}

	// 3.B The first integer in the triangle index buffer is the number of bytes to follow.
	// The bytes that follow are 2 byte integers, 16 bits.
	//  hence we divide this by 2 for a final vertex count.
	// Reinterpreting the char pointer as an int16 pointer then dereferencing it for value
	triangle_count =
		*(reinterpret_cast<int*>(MI16_buffer_location.start_location)) / 2;
	// Sanity check triplets
	if (triangle_count % 3)
	{
		std::cout << "Triangle count " << triangle_count << " not multiple of 3\n";
		return false;
	}
	// Initialize our triangle buffer
	indicies = std::make_unique<int16_t[]>(triangle_count);
	// Copy from the file buffer as int16_t
	vertex_index = 0;
	for (char* index = MI16_buffer_location.start_location + 4;	// Skip triangle count
		index < MI16_buffer_location.end_location;
		index += 2,			// Iterate two chars at a time
		vertex_index++)		// Iterate storage index
	{
		indicies[vertex_index] = *(reinterpret_cast<int16_t*> (index));
	}
	return true;
}

/* @breif Outputs current mesh data as obj file
   @param Path to save file to
   @return False if no mesh data or file unable to be written to
*/
bool TVM_mesh::write_to_obj(const std::string& a_path_and_file_name)
{
	// Do we have mesh data?
	if (verticies == nullptr || indicies == nullptr)
	{
		std::cout << "No mesh data to write to file " << a_path_and_file_name << "\n";
		return false;
	}

	//** Step 1 - Open and / or create the file **//
	// Output file stream object only needed during saving
	// -Given a unique ofsteam for threading
	std::ofstream out_file_stream;
	// Writing as raw text, no flags required
	out_file_stream.open(a_path_and_file_name);
	// Check we have file access
	if (!out_file_stream.is_open())
	{
		std::cout << "File unable to be written: " << a_path_and_file_name << "\n";
		return false;
	}

	//** Step 2 - Write all data as text, formatted as per wavefront obj schema
	// http://www.martinreddy.net/gfx/3d/OBJ.spec
	// Verticies are preceded with v and space delimited
	for (int i = 0; i < vertex_count; ++i)
	{
		out_file_stream << "v "
			<< verticies[i].position[0] << " "
			<< verticies[i].position[1] << " "
			<< verticies[i].position[2]
			<< "\n";
	}
	// UV's are preceded with vt and space delimited
	// Truevision's internal texture bitorder is inverted along V / Y
	// so we invert it as per v = f(1 - v);
	for (int i = 0; i < vertex_count; ++i)
	{
		out_file_stream << "vt "
			<< verticies[i].uv[0] << " "
			<< 1 - verticies[i].uv[1] // v inversion
			<< "\n";
	}
	// vertex normals are preceded with vn and space delimited
	for (int i = 0; i < vertex_count; ++i)
	{
		out_file_stream << "vn "
			<< verticies[i].normal[0]<< " "
			<< verticies[i].normal[1]<< " "
			<< verticies[i].normal[2]
			<< "\n";
	}
	// triangle indexing is an 'one' based array in obj,
	// so we must add one to all indicies while writing.
	for (int i = 0; i < triangle_count; i += 3)
	{
		out_file_stream << "f "
			<< indicies[i] + 1 << "/"
			<< indicies[i] + 1 << "/"
			<< indicies[i] + 1
			<< " "
			<< indicies[i + 1] + 1 << "/"
			<< indicies[i + 1] + 1 << "/"
			<< indicies[i + 1] + 1 
			<< " "
			<< indicies[i + 2] + 1 << "/"
			<< indicies[i + 2] + 1 << "/"
			<< indicies[i + 2] + 1
			<< "\n";
	}

	// Close the file, all done here
	out_file_stream.close();
	return true;
}

/* @brief Initializes the sentinals
   @param File buffer by reference
   @param The size of the file buffer in bytes
   @return False if any sentinels fail to initialize
*/
bool TVM_mesh::initialze_sentinels(std::unique_ptr<char[]>& a_file_buffer,
								   const size_t a_file_length_bytes)
{
	// Populate the vertex sentinel
	if (!find_delimiters(MVER_buffer_location, MVER, a_file_buffer, a_file_length_bytes))
	{
		std::cout << "Vertex buffer not found\n";
		return false;
	}
	// Populate the index sentinel
	if (!find_delimiters(MI16_buffer_location, MI16, a_file_buffer, a_file_length_bytes))
	{
		std::cout << "Indicies buffer not found\n";
		return false;
	}
	return true;
}

/* @brief look though the file buffer for the matching string location
   @param The sentinel to populate
   @param The index of the 'start' delimiter
   @param Pointer to the file buffer
   @param Length of the file buffer
   @return false if unsuccessful in finding the inner buffer
*/
bool TVM_mesh::find_delimiters(sentinel& a_sentinal_data,
							   const delimiter_index a_index,
							   std::unique_ptr<char[]>& a_file_buffer,
							   const size_t a_file_length_bytes)
{
	// Flag for whether we are searching for the start or end of this buffer
	bool found_buffer_start = false;
	// Go though the whole buffer
	for (int file_buffer_index = 0;
		file_buffer_index < a_file_length_bytes;
		++file_buffer_index)
	{
		// Looking for first delimiter
		if (!found_buffer_start)
		{
			// Character comparison of delimiter at buffer position
			for (int delimiter_index = 0;
				delimiter_index < delimiters[a_index].length();
				++delimiter_index)
			{
				// Character mismatch, stop comparison at this location
				if (a_file_buffer[file_buffer_index + delimiter_index] != delimiters[a_index][delimiter_index]) break;
				// Comparison matched through to last character, mark this location
				else if (delimiter_index == delimiters[a_index].length() - 1)
				{
					// This sentinals start address is the first char past the delimiter
					a_sentinal_data.start_location = &a_file_buffer[file_buffer_index + delimiters[a_index].length()]; // Return address one past delimiter
					// flip the flag to continue search for the closing delimiter
					found_buffer_start = true;
				}
			}
		}
		else // we are now looking for the closing delimiter
		{
			// Character comparison of delimiter at buffer position
			for (int delimiter_index = 0;
				// note a_index +1: we want the next delimiter
				delimiter_index < delimiters[a_index + 1].length();
				++delimiter_index)
			{
				// Character mismatch, stop comparison at this location
				if (a_file_buffer[file_buffer_index + delimiter_index] != delimiters[a_index + 1][delimiter_index]) break;
				// Comparison matched through to last character, mark this location
				else if (delimiter_index == delimiters[a_index + 1].length() - 1)
				{
					// This sentinals end address is the first char of the next delimiter
					a_sentinal_data.end_location = &a_file_buffer[file_buffer_index];
					// Calculate the length
					a_sentinal_data.length = 
						a_sentinal_data.end_location - 
						a_sentinal_data.start_location;
					// Return success
					return true;
				}
			}
		}
	}
	// Warn and fail.
	std::cout << "Sentinels for " << delimiters[a_index] << " not found\n";
	return false;
}

/* @brief Known ORDERED list of char based delimiters from the TVM format
	stored as a vector
*/
const std::vector<std::string> TVM_mesh::delimiters 
{
	std::string("MFOR"),
	std::string("MSTR"),
	std::string("MSTA"),
	std::string("MVER"),	// Verticies start here
	std::string("MI16"),	// Triangle list starts here
	std::string("MATT"),
	std::string("MGRO"),
	std::string("MGR3"),
	std::string("MBOU"),
	std::string("MEND")
};

/* vertex.h
@brief A simple union for different casts
of the binary vertex data in a TVM file.

Ultimately contains the position, normal and texture
coordinates of a single vertex.

Adam Clarke - 2018 10 11
*/

#pragma once
#ifndef VERTEX_H
#define VERTEX_H

static const size_t floats_per_vertex = 8;
static const size_t size_of_float = 4;
static const size_t size_of_vertex = floats_per_vertex * size_of_float;

union vertex
{
	// Raw floats from binary
	float float_data[floats_per_vertex];
	// The functional vertex structure
	struct
	{
		float position[3];
		float normal[3];
		float uv[2];
	};
};

#endif // !VERTEX_H
#Requires C++17

Projects are VS2017 format

This project converts binary TVMs (Truevision3D model) to .obj files though the command line.
It can convert single files or whole folders.
File path arguments are relative to the executable.
Run "TVM_to_OBJ -h" after compilation for the usage help.

[This work is licensed under a Creative Commons Attribution 3.0 Australia License](http://creativecommons.org/licenses/by/3.0/au/)